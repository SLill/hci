package com.example.f10c1.myfitness;

// Will listen to step alerts
public interface StepListener {

    public void step(long timeNs);

}
