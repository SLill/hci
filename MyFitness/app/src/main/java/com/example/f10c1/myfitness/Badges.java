package com.example.f10c1.myfitness;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class Badges extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener {

    GridView grid;
    String[] badgeTextA = {
            "Improver",
            "Streaker",
            "Action Man",
            "10K Pro",
            "Walkman"
    } ;
    int[] imageIdA = {
            R.drawable.improvement,
            R.drawable.streak,
            R.drawable.actionman,
            R.drawable.tenthousand,
            R.drawable.walkerman
    };

    String[] badgeTextB = {
            "Streaker",
            "10K Pro",
            "Walkman"
    } ;
    int[] imageIdB = {
            R.drawable.streak,
            R.drawable.tenthousand,
            R.drawable.walkerman
    };

    private Spinner datasetSpinner;
    private static final String[] paths = {"Dataset A", "Dataset B"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badges);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Badge Custom GridView
        setNewGrid(badgeTextA, imageIdA);

        // Dataset spinner
        datasetSpinner = (Spinner)findViewById(R.id.data_spinner);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(Badges.this,
                android.R.layout.simple_spinner_item,paths);

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        datasetSpinner.setAdapter(adapter2);
        datasetSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent i = new Intent(Badges.this, MainMenu.class);
            startActivity(i);
        } else if (id == R.id.nav_calendar) {
            Intent i = new Intent(Badges.this, Calendar.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setNewGrid(String[] badgeText, int[] imageId) {
        // Badge Custom GridView
        CustomGrid adapter = new CustomGrid(Badges.this, badgeText, imageId);
        grid=(GridView)findViewById(R.id.gridView);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position) {
                    case 0:
                        Toast.makeText(Badges.this, "Your average step count improved this month!", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(Badges.this, "You won a badge for 7 days in a row!", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(Badges.this, "You were active for over 90% of August!", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        Toast.makeText(Badges.this, "You walked 10,000 steps on a valid day!", Toast.LENGTH_SHORT).show();
                        break;
                    case 4:
                        Toast.makeText(Badges.this, "You were active for over 50% of July!", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(Badges.this, "Badge not found", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                setNewGrid(badgeTextA, imageIdA);
                break;
            case 1:
                setNewGrid(badgeTextB, imageIdB);
                break;
            default:
                setNewGrid(badgeTextA, imageIdA);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        setNewGrid(badgeTextA, imageIdA);
    }
}
