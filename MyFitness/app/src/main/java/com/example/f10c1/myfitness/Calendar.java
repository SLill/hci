package com.example.f10c1.myfitness;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.AdapterView.*;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Calendar extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnItemSelectedListener {

    CompactCalendarView compactCalendar;
    private SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MMMM- yyyy", Locale.getDefault());

    private Spinner datasetSpinner;
    private static final String[] paths = {"Dataset A", "Dataset B"};

    BarChart barChart;
    BarData barDataA;
    BarData barDataB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(null);

        compactCalendar = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        compactCalendar.setUseThreeLetterAbbreviation(true);
        compactCalendar.setEventIndicatorStyle(1);

        //Set an event for Teachers' Professional Day 2016 which is 21st of October

        Event ev1 = new Event(Color.GREEN, 1508360400000L, "You were active on this day");
        compactCalendar.addEvent(ev1);
        Event ev2 = new Event(Color.GREEN, 1508153559000L, "You were active on this day");
        compactCalendar.addEvent(ev2);
        Event ev3 = new Event(Color.GREEN, 1508067159000L, "You were active on this day");
        compactCalendar.addEvent(ev3);
        Event ev4 = new Event(Color.GREEN, 1507807959000L, "You were active on this day");
        compactCalendar.addEvent(ev4);
        Event ev5 = new Event(Color.GREEN, 1507548759000L, "You were active on this day");
        compactCalendar.addEvent(ev5);
        Event ev6 = new Event(Color.GREEN, 1507462359000L, "You were active on this day");
        compactCalendar.addEvent(ev6);
        Event ev7 = new Event(Color.GREEN, 1507375959000L, "You were active on this day");
        compactCalendar.addEvent(ev7);
        Event ev8 = new Event(Color.GREEN, 1507289559000L, "You were active on this day");
        compactCalendar.addEvent(ev8);
        Event ev9 = new Event(Color.GREEN, 1507203159000L, "You were active on this day");
        compactCalendar.addEvent(ev9);
        Event ev10 = new Event(Color.GREEN, 1507116759000L, "You were active on this day");
        compactCalendar.addEvent(ev10);
        Event ev11 = new Event(Color.GREEN, 1506598359000L, "You were active on this day");
        compactCalendar.addEvent(ev11);
        Event ev12 = new Event(Color.GREEN, 1506079959000L, "You were active on this day");
        compactCalendar.addEvent(ev12);
        Event ev13 = new Event(Color.GREEN, 1505388759000L, "You were active on this day");
        compactCalendar.addEvent(ev13);
        Event ev14 = new Event(Color.GREEN, 1504265559000L, "You were active on this day");
        compactCalendar.addEvent(ev14);
        Event ev15 = new Event(Color.GREEN, 1508417962000L, "You were active on this day");
        compactCalendar.addEvent(ev15);

        compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                Context context = getApplicationContext();

                List<Event> events = compactCalendar.getEvents(dateClicked);
                if (events.size() > 0) {
                    Toast.makeText(context, events.get(0).getData().toString(), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(context, "Not an active day", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                actionBar.setTitle(dateFormatMonth.format(firstDayOfNewMonth));
            }
        });

        // weekly data
        // bar chart step data
        barChart = (BarChart) findViewById(R.id.week_bargraph);

        // Dataset A
        ArrayList<BarEntry> barEntriesA = new ArrayList<>();
        barEntriesA.add(new BarEntry(0f, 8340f));
        barEntriesA.add(new BarEntry(1f, 9212f));
        barEntriesA.add(new BarEntry(2f, 11674f));
        barEntriesA.add(new BarEntry(3f, 10986f));

        BarDataSet dataSetA = new BarDataSet(barEntriesA, "Weeks");
        dataSetA.setHighlightEnabled(true); // allow highlighting for DataSet
        dataSetA.setHighLightColor(Color.YELLOW);
        dataSetA.setHighLightAlpha(200);
        dataSetA.setColors(new int[] {Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE});

        barDataA = new BarData(dataSetA);
        barDataA.setBarWidth(0.75f); // set custom bar width

        // Dataset B
        ArrayList<BarEntry> barEntriesB = new ArrayList<>();
        barEntriesB.add(new BarEntry(0f, 6295f));
        barEntriesB.add(new BarEntry(1f, 7125F));
        barEntriesB.add(new BarEntry(2f, 5860f));
        barEntriesB.add(new BarEntry(3f, 8391f));

        BarDataSet dataSetB = new BarDataSet(barEntriesB, "Weeks");
        dataSetB.setHighlightEnabled(true); // allow highlighting for DataSet
        dataSetB.setHighLightColor(Color.YELLOW);
        dataSetB.setHighLightAlpha(200);
        dataSetB.setColors(new int[] {Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE});

        barDataB = new BarData(dataSetB);
        barDataB.setBarWidth(0.75f); // set custom bar width

        // default is Dataset A
        barChart.setData(barDataA);

        // X Axis formatting
        final String[] weeks = new String[] { "W1", "W2", "W3", "W4"};

        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return weeks[(int) value];
            }
        };

        XAxis xAxis = barChart.getXAxis();
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        // Y Axis formatting
        YAxis yAxisLeft = barChart.getAxisRight();
        yAxisLeft.setDrawGridLines(false);
        yAxisLeft.setDrawAxisLine(false);

        YAxis yAxisRight = barChart.getAxisRight();
        yAxisRight.setEnabled(false);

        barChart.setTouchEnabled(false);
        barChart.setScaleEnabled(true);
        //default highlight for dataset A
        barChart.highlightValue(3f, 0);
        barChart.setAutoScaleMinMaxEnabled(true);
        barChart.setNoDataText("Not Recorded Yet");

        Description desc = new Description();
        desc.setText("");
        barChart.setDescription(desc);

        barChart.getLegend().setEnabled(false);

        barChart.animateXY(3000, 3000);

        barChart.invalidate(); // refresh

        // Dataset spinner
        datasetSpinner = (Spinner)findViewById(R.id.week_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Calendar.this,
                android.R.layout.simple_spinner_item,paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        datasetSpinner.setAdapter(adapter);
        datasetSpinner.setOnItemSelectedListener(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent i = new Intent(Calendar.this, MainMenu.class);
            startActivity(i);
        } else if (id == R.id.nav_badges) {
            Intent i = new Intent(Calendar.this, Badges.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                barChart.setData(barDataA);
                barChart.highlightValue(3f, 0);
                barChart.animateXY(3000, 3000);
                barChart.invalidate(); // refresh
                break;
            case 1:
                barChart.setData(barDataB);
                barChart.highlightValue(3f, 0);
                barChart.animateXY(3000, 3000);
                barChart.invalidate(); // refresh
                break;
            default:
                barChart.setData(barDataA);
                barChart.highlightValue(3f, 0);
                barChart.animateXY(3000, 3000);
                barChart.invalidate(); // refresh
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        barChart.setData(barDataA);
        barChart.highlightValue(3f, 0);
        barChart.animateXY(3000, 3000);
        barChart.invalidate(); // refresh
    }

}
