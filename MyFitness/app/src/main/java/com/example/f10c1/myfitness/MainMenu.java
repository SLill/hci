package com.example.f10c1.myfitness;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.AdapterView.*;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;


public class MainMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SensorEventListener, StepListener, OnItemSelectedListener {

    BarChart barChart;
    BarData barDataA;
    BarData barDataB;

    private TextView dailyStepText;
    ImageButton BtnStart;
    ImageButton BtnStop;
    private StepDetector simpleStepDetector;
    private SensorManager sensorManager;
    private Sensor accel;
    private int numSteps = 6852;


    private Spinner datasetSpinner;
    private static final String[]paths = {"Dataset A", "Dataset B"};

    private ImageView validImage;
    private TextView validText;

    TextView averageStepText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        validImage = (ImageView) findViewById(R.id.validImage);
        validText = (TextView) findViewById(R.id.validText);
        averageStepText = (TextView) findViewById(R.id.averageStepText);

        // step counter
        // Get an instance of the SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);

        dailyStepText = (TextView) findViewById(R.id.dailystepText);
        BtnStart = (ImageButton) findViewById(R.id.btn_start);
        BtnStop = (ImageButton) findViewById(R.id.btn_stop);

        // start automatically
        sensorManager.registerListener(MainMenu.this, accel, SensorManager.SENSOR_DELAY_FASTEST);

        BtnStart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

            }
        });

        BtnStart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setBackgroundColor(Color.rgb(240,240,240));
                    sensorManager.registerListener(MainMenu.this, accel, SensorManager.SENSOR_DELAY_FASTEST);
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    v.setBackgroundColor(Color.rgb(250,250,250));
                }
                return false;
            }
        });

        BtnStop.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setBackgroundColor(Color.rgb(240,240,240));
                    sensorManager.unregisterListener(MainMenu.this);
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    v.setBackgroundColor(Color.rgb(250,250,250));
                }
                return false;
            }
        });

        // bar chart step data
        barChart = (BarChart) findViewById(R.id.bargraph);

        // Dataset A
        ArrayList<BarEntry> barEntriesA = new ArrayList<>();
        barEntriesA.add(new BarEntry(0f, 7340f));
        barEntriesA.add(new BarEntry(1f, 13050f));
        barEntriesA.add(new BarEntry(2f, 12395f));
        barEntriesA.add(new BarEntry(3f, 8903f));
        barEntriesA.add(new BarEntry(4f, 0f));
        barEntriesA.add(new BarEntry(5f, 0f));
        barEntriesA.add(new BarEntry(6f, 0f));

        BarDataSet dataSetA = new BarDataSet(barEntriesA, "Days");
        dataSetA.setHighlightEnabled(true); // allow highlighting for DataSet
        dataSetA.setHighLightColor(Color.YELLOW);
        dataSetA.setHighLightAlpha(200);
        dataSetA.setColors(new int[] {Color.RED, Color.GREEN, Color.GREEN, Color.GREEN});

        barDataA = new BarData(dataSetA);
        barDataA.setBarWidth(0.75f); // set custom bar width

        // Dataset B
        ArrayList<BarEntry> barEntriesB = new ArrayList<>();
        barEntriesB.add(new BarEntry(0f, 6295f));
        barEntriesB.add(new BarEntry(1f, 3789f));
        barEntriesB.add(new BarEntry(2f, 4123f));
        barEntriesB.add(new BarEntry(3f, 5679f));
        barEntriesB.add(new BarEntry(4f, 3129f));
        barEntriesB.add(new BarEntry(5f, 5931f));
        barEntriesB.add(new BarEntry(6f, 0f));

        BarDataSet dataSetB = new BarDataSet(barEntriesB, "Days");
        dataSetB.setHighlightEnabled(true); // allow highlighting for DataSet
        dataSetB.setHighLightColor(Color.YELLOW);
        dataSetB.setHighLightAlpha(200);
        dataSetB.setColors(new int[] {Color.RED, Color.RED, Color.GREEN, Color.RED, Color.RED, Color.GREEN});

        barDataB = new BarData(dataSetB);
        barDataB.setBarWidth(0.75f); // set custom bar width

        // default is Dataset A
        barChart.setData(barDataA);

        // X Axis formatting
        final String[] days = new String[] { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return days[(int) value];
            }
        };

        XAxis xAxis = barChart.getXAxis();
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        // Y Axis formatting
        YAxis yAxisLeft = barChart.getAxisRight();
        yAxisLeft.setDrawGridLines(false);
        yAxisLeft.setDrawAxisLine(false);

        YAxis yAxisRight = barChart.getAxisRight();
        yAxisRight.setEnabled(false);

        barChart.setTouchEnabled(false);
        barChart.setScaleEnabled(true);
        //default highlight for dataset A
        barChart.highlightValue(3f, 0);
        barChart.setAutoScaleMinMaxEnabled(true);
        barChart.setNoDataText("Not Recorded Yet");

        Description desc = new Description();
        desc.setText("");
        barChart.setDescription(desc);

        barChart.getLegend().setEnabled(false);

        barChart.animateXY(3000, 3000);

        barChart.invalidate(); // refresh

        // Dataset spinner
        datasetSpinner = (Spinner)findViewById(R.id.dataset_spinner);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(MainMenu.this,
                android.R.layout.simple_spinner_item,paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        datasetSpinner.setAdapter(adapter);
        datasetSpinner.setOnItemSelectedListener(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_calendar) {
            Intent i = new Intent(MainMenu.this, Calendar.class);
            startActivity(i);
        } else if (id == R.id.nav_badges) {
            Intent i = new Intent(MainMenu.this, Badges.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void step(long timeNs) {
        numSteps++;
        dailyStepText.setText(Integer.toString(numSteps));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                barChart.setData(barDataA);
                barChart.highlightValue(3f, 0);
                barChart.animateXY(3000, 3000);
                barChart.invalidate(); // refresh
                numSteps = 6852;
                dailyStepText.setText(Integer.toString(numSteps));
                validImage.setImageResource(R.mipmap.tick);
                validText.setText("10/10 active hours\nToday is a active day\nNice Work!");
                averageStepText.setText("11,449\navg.steps");
                break;
            case 1:
                barChart.setData(barDataB);
                barChart.highlightValue(5f, 0);
                barChart.animateXY(3000, 3000);
                barChart.invalidate(); // refresh
                numSteps = 3102;
                dailyStepText.setText(Integer.toString(numSteps));
                validImage.setImageResource(R.mipmap.cross);
                validText.setText("3/10 active hours\n7 more to go!");
                averageStepText.setText("7,423\navg.steps");
                break;
            default:
                barChart.setData(barDataA);
                barChart.highlightValue(3f, 0);
                barChart.animateXY(3000, 3000);
                barChart.invalidate(); // refresh
                numSteps = 6852;
                dailyStepText.setText(Integer.toString(numSteps));
                validImage.setImageResource(R.mipmap.tick);
                validText.setText("10/10 active hours\nToday is a active day\nNice Work!");
                averageStepText.setText("11,449\navg.steps");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        barChart.setData(barDataA);
        barChart.highlightValue(3f, 0);
        barChart.animateXY(3000, 3000);
        barChart.invalidate(); // refresh
        numSteps = 6852;
        dailyStepText.setText(Integer.toString(numSteps));
        validImage.setImageResource(R.mipmap.tick);
        validText.setText("10/10 active hours\nToday is a active day\nNice Work!");
        averageStepText.setText("11,449\navg.steps");
    }
}
